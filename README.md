# Madgraph

Project collecting the necessary data for running Madgraph with Gauss.

## [gridpacks](gridpacks)

Gridpacks for MadGraph generation are stored here. Events are
generated with MadGraph utilizing the "gridpack" method for MadGraph
5:

```
https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/GridDevelopment
```
 
and an eqivalent method for aMC@NLO:
 
```
https://answers.launchpad.net/mg5amcnlo/+question/243268
```
 
The run directory is set as 
 
```bash
<event number>_<beam 1 energy in GeV>_<beam 2 energy in GeV>
```
 
and does not need to be deleted between independent runs with the same
configuration (excluding random seeds). Indeed, keeping the directory
significantly speeds the generation process, particularly for NLO
generation with aMC@NLO as the grid initialization can be skipped
after the initial run.

Gridpacks can be generated with the `cmt/gridpack.sh` script as
follows. Note that here, the Gauss version does not matter, but rather
the `CMTCONFIG`, Madgraph version, event type, and beam energies.
NOTE: It's assumed the event type is developed for MadgraphProduction scheme ([see example](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/-/blob/brachwal/refac_Z_mumubb_Madgraph/dkfiles/Z_mumubb_Madgraph.dec))

```bash
# Clone this repository and create your branch
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/Gen/MadgraphData.git
cd MadgraphData; git checkout -b ${USER}/<event type>

# Run the script. This generates the gridpack and moves it to 
# gridpacks/<CMTCONFIG>.
lb-run -c <CMTCONFIG> Gauss/<version> cmt/gridpack.sh <beam configuration> <event type options>

# Add the gridpack and commit.
git add gridpacks/${CMTCONFIG}
git commit -m "Added gridpack for <event type>."
git push -u origin ${USER}/<event type>

# Make a merge request for the gridpack on gitlab.
```

The gridpack generation script requires the use of `strace` to
significantly reduce the size of the gridpack (by orders of
magnitude). If running the script within Docker, additional system
privileges must be escalated. This can be done by passing either the
flag `--cap-add=SYS_PTRACE` or `--privileged`.

A working example is given below. MadGraph is included in Sim10 series (Gauss v55r4 and later). 
To prepare gridpacks, use latest corresponding Gauss release (in the example below we use v55r4) 
unless new functionality or new platform are needed in which case you can use nightly build in lhcb-gauss-dev slot. 
```bash
# Set the enviroment and Gauss locally
lb-set-platform x86_64_v2-centos7-gcc11-opt
lb-dev Gauss/v55r4
cd GaussDev_v55r4; make;
./build.${CMTCONFIG}/run bash

# Run, here '70000000.py' is the dummy event type.
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/Gen/MadgraphData.git
source MadgraphData/cmt/gridpack.sh '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$LBMADGRAPHROOT/options/70000000.py'

# This creates '70000000_6500_6500.tgz'.
ls MadgraphData/gridpacks/${CMTCONFIG}

# Remove the run directory to test if things worked.
rm -r 70000000_6500_6500/

# To use this gridpack, the MADGRAPHDATAROOT path must be set.
export MADGRAPHDATAROOT=$PWD/MadgraphData
source '$LBMADGRAPHROOT/options/example_job.sh'
```

## [modules](modules)

Additional modules can be provided to MadGraph, whether models or
other add-ons. The `modules` directory is automatically added to the
Python path before calling Madgraph. Each module should be given its
own directory.
