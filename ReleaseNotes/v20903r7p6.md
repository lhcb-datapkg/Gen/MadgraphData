2024-11-07 MadgraphData v20903r7p6
======

This version is released on 'master' branch.

### Enhancements  ~enhancement

- Corrected LO WW2mue gridpack, see !29 (@ganowak)
