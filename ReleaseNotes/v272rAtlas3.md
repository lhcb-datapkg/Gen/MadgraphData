2020-09-16 MadgraphData v1r0
========================================

This version is released on 'master' branch.

- Added ALPs gridpacks (DecFiles!518), see !7 and !9 (@acasaisv).
  - 49100040: ALP5 -> gamma gamma
  - 49100041: ALP6 -> gamma gamma
  - 49100042: ALP7 -> gamma gamma
  - 49100043: ALP8 -> gamma gamma
  - 49100044: ALP9 -> gamma gamma
  - 49100045: ALP10 -> gamma gamma
  - 49100046: ALP11 -> gamma gamma
  - 49100048: ALP4 -> gamma gamma
  - 49100049: ALP4.5 -> gamma gamma
- Added HNL gridpack (DecFiles!538), see !4 (@mmarinan)
  - 49612000: ~chi_30 -> e- mu+ Neutrino
- Introduce requirement files for versioning. This is necessary so that when building the package versioning of the package is done, see !3 (@gcorti)
- Added Z to bbar gridpack ( DecFiles!523), see !2 (@brachwal).
  - 42912009: pp -> (Z-> mu mu) (b b~)
- Added R-axion model. Added new model for R-axion. !1 (@philten)
