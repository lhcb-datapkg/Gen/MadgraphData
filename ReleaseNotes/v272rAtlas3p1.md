2021-10-02 MadgraphData v272rAtlas3p1
========================================

This version is released on 'master' branch.

- Added Z to bbar gridpack for x86_64_v2-centos7-gcc10-opt architecture, see !10 (@brachwal, @kreps).
  - 42912009: pp -> (Z-> mu mu) (b b~)
- Addapted gridpack.sh script to work with recent Gauss, see !10 (@kreps).

