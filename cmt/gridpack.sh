#!/usr/bin/env bash
# Usage: "./gridpack.sh <beam configuration> <event type options>"
# Generates a gridpack for a given beam configuration and decay file.
# Requires the use of strace. To enable within Docker, use either the
# "--cap-add=SYS_PTRACE" or "--privileged" options.
# Written by Philip Ilten, August 2020.

# Determine the top of this package.
TOP=$(dirname "$(readlink -f "$0")")

datatype=`basename $1|cut -d '-' -f3|grep -E ^\-?[0-9]+$`
if [ "$datatype" = "" ]; then
  datatype=`basename $1|cut -d '-' -f4|grep -E ^\-?[0-9]+$`
  if [ "$datatype" = "" ]; then
    datatype='Upgrade'
  fi
fi

# Create the configuration.
echo "Creating the gridpack configuration."
CFG=gridpack.py
cat > $CFG << BLOCKTEXT
from Configurables import LHCbApp, OutputStream, Gauss
from Configurables import Generation, Special, MadgraphProduction
OutputStream("GaussTape").Output = (
    "DATAFILE='PFN:gridpack.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 1
Gauss().Phases = ["Generator", "GenToMCTree"]
Gauss().SampleGenerationToolOptions.update({"GenGridpack":True, "EvtMax" : 1})
Gauss().DataType  = "$datatype"
BLOCKTEXT

# Run the generation.
echo "Generating the gridpack."
gaudirun.py $1 $LBMADGRAPHROOT/options/MadgraphPythia8.py $2 $CFG

# Determine the package location.
echo "Finding the gridpack location."
GRID=`ls -td [0-9]*/ | head -n 1`
GRID=${GRID::-1}
if [ -z "GRID" ]; then
    >&2 echo "Cannot find generated gridpack directory."; exit 1; fi

# Determine necessary files for LO generation using strace.
if [ -d $GRID/madevent ]; then
    TMP=$GRID.TMP && mkdir $TMP

    # Rerun and trace file access.
    echo "Determining LO gridpack dependencies."
    sed -i "s/-Generation/Generation/g" $CFG
    DEPS=`strace -ff -e trace=file\
         gaudirun.py $1 $LBMADGRAPHROOT/options/MadgraphPythia8.py $2 $CFG 2>&1\
         | perl -ne 's/^[^"]+"(([^\\"]|\\[\\"nt])*)".*/$1/ && print'\
         | grep "^$PWD/$GRID" | sort -u`
    if [ -z "DEPS" ]; then
	>&2 echo "Cannot determine gridpack dependencies."; exit 1; fi

    # Keep only files in the trace.
    echo "Keeping only necessary LO gridpack files."
    for DEP in $DEPS; do
	if [ $DEP == $PWD/$GRID ]; then continue; fi
	FILE=$TMP/${DEP#"$PWD/$GRID/"}
	if [ -d $DEP ]; then mkdir -p $FILE
	elif [ -f $DEP ]; then
	    if [ ! -d `dirname $FILE` ]; then mkdir -p `dirname $FILE`; fi
	    cp $DEP $FILE; fi; done
    cp $GRID/run.sh $TMP/run.sh
    rm -rf $TMP/Events/* $TMP/*.gz
    mv $GRID $GRID.ORIGINAL
    mv $TMP $GRID

# Remove unneeded files for NLO generation manually since strace method fails.
else
    echo "Removing unneeded NLO gridpack files."
    rm -rf $GRID/amcatnlo.tar.gz $GRID/lib/Pdfdata
    rm -rf $GRID/Source/IREGI $GRID/Source/CutTools $GRID/Source/StdHEP
    rm -rf `find $GRID -name "events.lhe"`
    rm -rf `find $GRID -name "check_poles"`
    rm -rf `find $GRID -name "test_soft_col_limits"`
    rm -rf `find $GRID -name "log*"`
    rm -rf `find $GRID -name "*.log"`
    rm -rf `find $GRID -name "*.mod"`
    rm -rf `find $GRID -name "*.[fF]"`
    rm -rf `find $GRID -name "*.f90"`
    rm -rf `find $GRID -name "*.cc"`
    rm -rf `find $GRID -name "*.hh"`
    rm -rf `find $GRID -name "*.h"`
    rm -rf `find $GRID -name "*.html"`
    rm -rf `find $GRID -name "*.ps"`
    rm -rf `find $GRID -name "*.o"`; fi

# Add soft link to PDF sets.
echo "Soft linking PDF sets."
for FILE in `find $GRID -name PDFsets`; do
    rm -r $FILE; ln -s $LHAPDF_DATA_PATH $FILE; done

# Switch run mode and number of cores.
echo "Configuring run mode and number of cores."
for CFG in `find $GRID -name '*_configuration.txt'`; do
    CORE=`grep ^nb_core $CFG`
    if [ -n "$CORE" ]; then sed -i 's/nb_core[ ]*=.*/nb_core = 1/g' $CFG
    else echo 'nb_core = 1' >> $CFG; fi
    MODE=`grep ^run_mode $CFG`
    if [ -n "$MODE" ]; then sed -i 's/run_mode[ ]*=.*/run_mode = 0/g' $CFG
    else echo 'run_mode = 0' >> $CFG; fi;
    HTML=`grep ^automatic_html_opening $CFG`
    if [ -n "$HTML" ]; then sed -i 's/automatic_html_opening[ ]*=.*/automatic_html_opening = False/g' $CFG
    else echo 'automatic_html_opening = False' >> $CFG; fi; 
done


# Create the zipped tarball and move.
echo "Compressing final packaged gridpack."
mkdir -p $TOP/../gridpacks/$CMTCONFIG/
tar -cvf - $GRID | gzip --best > $TOP/../gridpacks/$CMTCONFIG/$GRID.tgz
