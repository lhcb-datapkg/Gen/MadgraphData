# This file was automatically created by FeynRules 2.0.30
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (November 20, 2012)
# Date: Mon 3 Apr 2017 00:19:04


from object_library import all_orders, CouplingOrder


DMS = CouplingOrder(name = 'DMS',
                    expansion_order = 2,
                    hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

